module ContentScript.Main where

import Prelude (Unit, append, bind, discard, pure, ($), (<<<), (<>), (>>=), (>>>))

import Effect.Now (nowDateTime)
import Data.DateTime
import Data.RFC3339String (RFC3339String(..), fromDateTime)

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))

import Data.Argonaut.Core as J
import Data.Argonaut.Encode (encodeJson)

import Affjax.Web (Error, Response, Request, URL, defaultRequest, printError, request)

import Affjax.RequestBody as RequestBody
import Affjax.RequestHeader (RequestHeader(..))
import Affjax.ResponseFormat as ResponseFormat

import Data.HTTP.Method (Method (POST))
import Data.MediaType.Common (applicationJSON)

import Effect.Aff (Aff, launchAff_)

import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Console (log)

import Browser.Document (getURL)
import Browser.Storage (Token, getStoredToken)


type IngressURI = {
    uri :: String
  , timestamp :: String
  }


mkHeaders :: Token → Array RequestHeader
mkHeaders t = [
    ContentType             applicationJSON
  , Accept                  applicationJSON
  , tokenAsHeader t
  ]


tokenAsHeader :: Token → RequestHeader
tokenAsHeader t = RequestHeader "X-Consumer-Custom-ID" t


baseUri :: URL
baseUri = "https://ingress.laibnits.com"


formatTimestamp :: DateTime -> String
formatTimestamp = fromDateTime >>> \(RFC3339String fts) → fts


encodeBody :: IngressURI → RequestBody.RequestBody
encodeBody = RequestBody.json <<< encodeJson


logger :: ∀ a. MonadEffect a => String -> a Unit
logger = liftEffect <<< log <<< append "laɪbnɪts - "


postRequest :: ∀ a. ResponseFormat.ResponseFormat a → Token → URL -> IngressURI -> Request a
postRequest fmt t url content = defaultRequest
  { url            = url
  , method         = Left POST
  , headers        = mkHeaders t
  , content        = Just (encodeBody content)
  , responseFormat = fmt
  }


makeRequest :: String -> IngressURI -> Aff (Either Error (Response J.Json))
makeRequest t u = request $ postRequest ResponseFormat.json t baseUri u


postUrI :: Token -> IngressURI -> Aff String
postUrI token ingressURI = do
  makeRequest token ingressURI >>= flattenResult
    where
      flattenResult :: Either Error (Response J.Json) → Aff String
      flattenResult r = pure case r of
        Left err        -> "FAILURE: Cannot upload URI to: " <> baseUri <> ": " <> printError err
        Right response  -> "SUCCESS: " <> (J.stringify response.body)


upload :: Either String Token -> DateTime → String -> Aff Unit
upload token ts pageUri = do
  case token of
    Left  err    → logger $ "FAILURE: " <> err
    Right token' → do
      logger $ "Uploading uri: " <> pageUri <> " to: " <> baseUri
      postUrI token' mkIngressURI >>= liftEffect <<< logger

  where
    mkIngressURI = { uri: pageUri, timestamp: formatTimestamp ts }


main :: Effect Unit
main = do
  launchAff_ do
    pageUri ← getURL
    token ← getStoredToken
    ts ← liftEffect nowDateTime

    upload token ts pageUri
