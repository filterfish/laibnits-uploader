"use strict"

const missing_token_message = "Token missing. Please add your token to the extension's preferences page."

export const getImpl = key => left => right => (err, response) => {
  const success = (t) => {
    return (t.token === undefined || t.token === "") ? response(left(missing_token_message)) : response(right(t.token));
  }

  const failure = (err) => {
    return response(left(err));
  }

  browser.storage.local.get(key).then(success, failure).catch(failure);
}
