module Browser.Storage (
    Token
  , getStoredToken
  ) where

import Data.Either (Either(..))
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)


type Token = String


foreign import getImpl :: ∀ a b. String → (∀ x y. x -> Either x y) -> (∀ x y. y → Either x y) -> EffectFnAff (Either a b)


getStoredToken :: Aff (Either String Token)
getStoredToken = fromEffectFnAff (getImpl "token" Left Right)
