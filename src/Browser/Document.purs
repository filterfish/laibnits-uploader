module Browser.Document (
    getURL
  ) where

import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)

foreign import getURLImpl :: EffectFnAff String


getURL :: Aff String
getURL = fromEffectFnAff getURLImpl
