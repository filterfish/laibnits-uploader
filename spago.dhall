{ name = "laibnits-uploader"
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
, dependencies =
  [ "aff"
  , "affjax"
  , "affjax-web"
  , "argonaut-codecs"
  , "argonaut-core"
  , "console"
  , "datetime"
  , "effect"
  , "either"
  , "functions"
  , "http-methods"
  , "maybe"
  , "media-types"
  , "now"
  , "precise-datetime"
  , "prelude"
  ]
}
