"use strict"

const isEmpty = (t) =>  (t === "" || t === undefined)
const fmtToken = (r) => r ? r: ""

const logE = (e) => console.log(`Error: ${e}`);

const getToken = (success) => { browser.storage.local.get("token").then(success, logE); }

const setToken = (value) => {
  if (isEmpty(value)) {
    browser.storage.local.remove("token").catch(logE);
  } else {
    browser.storage.local.set({"token": value}).catch(logE);
  }
}

const getDomToken = ()  => document.querySelector("#token").value;
const setDomToken = (t) => document.querySelector("#token").value = t;

document.addEventListener("DOMContentLoaded", () => {
  getToken(result => setDomToken(fmtToken(result.token)));
});

document.querySelector("form").addEventListener("submit", e => {
  e.preventDefault();
  setToken(getDomToken());
});
