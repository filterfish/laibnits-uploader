.PHONY: prod dev content background watcher lint clean really-clean

export PATH := $(PATH):./node_modules/.bin

version := $(shell jq -r .version extension/manifest.json)
src-dir := src

bundler := spago bundle-app
watcher := spago build --watch
builder := web-ext build
linter := web-ext lint

javascript-src := src/Browser/$(wildcard *.js)
extension-dir := extension

content-module := ContentScript.Main
content-src := ${src-dir}/ContentScript/Main.purs

background-module := BackgroundScript.Main
background-src := ${src-dir}/BackgroundScript/Main.purs

content-artifact := ${extension-dir}/scripts/content.js
background-artifact := ${extension-dir}/scripts/background.js

artifacts := ${content-artifact} ${background-artifact}

extension-zip := web-ext-artifacts/laibnits-${version}.zip
extension-xpi := /tmp/laibnits-${version}.xpi

export BUILD_ENVIRONMENT

ifeq ($(BUILD_ENVIRONMENT),dev)
flags := --source-maps
else
flags := --minify
endif


prod: BUILD_ENVIRONMENT = prod
prod:
	@$(MAKE) ${extension-zip}


dev: BUILD_ENVIRONMENT = dev
dev:
	@$(MAKE) ${extension-xpi}


watcher:
	${watcher}

lint:
	${linter} -s ${extension-dir}


clean:
	rm -f ${artifacts} ${extension-zip}


really-clean: clean
	rm -rf ${extension-xpi} output generated-docs


content: ${content-artifact}


background: ${background-artifact}


${content-artifact}: ${content-src} ${javascript-src}
	${bundler} --to=${content-artifact} ${flags} --main=${content-module}


${background-artifact}: ${background-src} ${javascript-src}
	${bundler} --to=${background-artifact} ${flags} --main=${background-module}


${extension-zip}: ${content-artifact} ${background-artifact}
	${builder} --overwrite-dest -s ${extension-dir}


${extension-xpi}: ${extension-zip}
	cp ${extension-zip} ${extension-xpi}
