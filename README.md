# README

This repo contains code for the laɪbnɪts URL upload extension. The extension
uploads the current URL to laibnits.com and where it is processed and added to
the user's document repository. The extension requires a key for authentication
which must be added in the extension's preference page.


## Building

The extension is written in [Purescript](https://purescript.org) which is built using
[spago](https://github.com/purescript/spago) and finally packaged using
[web-ext](https://github.com/mozilla/web-ext).


### Dependencies

#### System Dependencies

The only system dependencies are [nodejs](https://nodejs.org/) and
[npm](https://www.npmjs.com/) and it must be a fairly up to date version. See the
respective project websites for details. If you are using Debian testing or
unstable then the following command is enough:

    apt-get install nodejs npm

#### Javascript Dependencies

All dependencies are installed using `npm` with the following command:

    npm install

### Building the extension

To build the extension itself you simply need to run:

    make prod

This will compile the purescript and generate the extension zip file which can
be found in the `web-ext-artifacts` directory.

For development you can use:

    make dev

There are a number of other helpful targets in the `Makefile` so I suggest you
look there for more details, however some commonly used targets are
`watcher` (compiles the purescript whenever a file changes) and `lint`
(validates the extension's source).


## Installation

At the moment the extension must be installed by hand using the extension
manager with a development version of firefox or you can use `web-ext run` to
make life *much* easier when developing the extension. See the `web-ext`
documentation for details.


## Future Work

The extension is fairly basic in that it provides the basic functionality but still requires more work:

- [ ] Chrome plugin. This only works with Firefox but will obviously need to
  support Chrome and Chromium. I hope that the webextension-polyfill package
  will Just Work™ but I haven't had much success so far either with purescript
  or straight javascript so I'm assuming I've done something wrong.
- [ ] Blacklist. It's important for the user to be able to block sites that they
  don't want uploaded to laɪbnɪts
- [ ] Whitelist. In some instances the user may only want to upload pages from
  certain domains. Whilst this significantly limits the usefulness it's certainly
  a legitimate use-case.
- [ ] Upload the document itself. If the user needs to upload password protected
  pages then the extension will need to scrape the text and upload that. This
  however has quite significant ramifications and needs to be *very* carefully
  considered.
- [ ] Authentication. At the moment the extension uses a toke for
  authentication which is not considered to the current best-practice and
  should be replaced by OAuth. This requires support from the laɪbnɪts
  authentication server and that work has not yet been started.
